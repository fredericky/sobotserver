* all source files in the directory lib.
* mongodb.conf is the config file for MongoDB
* there are several scripts under the directory
  - runDB: start MongoDB
  - runDBClient:  start a MongoDB client
  - runSioServer: start a sioserver (long connection based for chat)
  - runWebserver: start a webserver for handling HTTP requests from the client
