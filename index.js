// the entry point of the project
var argv = require('optimist').argv;
var server;

switch(argv.s) {
  // start webserver for handling http request
  case 'webserver':
    server = require('./lib/webserver');
    break;
  // start sioserver for handling long connection based request
  case 'sioserver':
    server = require('./lib/sioserver');
    break;
  default:
    console.error('unknown server type' + argv.s);
    break;
}

var domain = require('domain').create();
domain.on('error', function(err) {
  console.error(err.stack);
});

domain.run(function() {
  server.start(argv);
});
