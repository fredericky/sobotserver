var async = require('async');
var mongodb = require('mongodb');

// constructor of chat service
function ChatService(opts) {
  this.store = opts.s;
  this.sockets = opts.sockets;
}

// fetch chat msg list
ChatService.prototype.fetchChatMsgList= function(msg, res) {
  console.log('chatservice:fetchChatMsgList:msg=', msg);
  var self = this;
  var reply = {
    ret : 0
  };
  async.series([
    function(c) {
      if(msg.msgType === 0) {
        var chatCol = self.store.getCollection('chat');
        if(msg.sinceId == 'none') {
          chatCol.find({receiver:msg.userId}).sort({_id:1}).limit(msg.length).toArray(function(e,arr){
            if(e) return c(e);
            data = arr;
            c();
          });
        } else {
          var objId = mongodb.ObjectID(msg.sinceId);
          chatCol.find({receiver:msg.userId, _id:{$gt:objId}}).limit(msg.length).toArray(function(e, arr) {
            if(e) return c(e);
            data = arr;
            c();
          });
        }
      } else if(msg.msgType === 1) {
        var groupChatCol = self.store.getCollection('groupchat');
        if(msg.sinceId == 'none') {
          groupChatCol.find({groupId:msg.groupId}).sort({_id:1}).limit(msg.length).toArray(function(e,arr){
            if(e) return c(e);
            data = arr;
            c();
          });
        } else {
          var objId2 = mongodb.ObjectID(msg.sinceId);
          groupChatCol.find({groupId:msg.groupId, _id:{$gt:objId2}}).limit(msg.length).toArray(function(e, arr) {
            if(e) return c(e);
            data = arr;
            c();
          });
        }
      } else {
        err = 2;
        reply.ret = err;
        return c(err);
      }
    },
    function(c) {
      if(data.length !== 0) {
        reply.lastId = data[data.length-1]._id;
      }
      data.forEach(function(e) {
        if(msg.msgType === 0) delete e.receiver;
        else if(msg.msgType === 1) delete e.groupId;
        delete e._id;
      });
      reply.msgList = data;
      c();
    }
  ],
  function(err) {
    res.send(reply);
  });

};


// send a msg including personal chat and group chat
ChatService.prototype.sendMsg = function(msg, socket) {
  console.log('chatservice:sendmsg=', msg);
  var self = this;
  var msgType = msg.msgType;
  if(msgType !== 0 && msgType !== 1) {
    socket.emit('msgReply', {ret:1});
    return;
  }
  if(msgType === 0 && msg.userId === msg.id) {
    socket.emit('msgReply', {ret:2});
    return;
  }
  var userCol = self.store.getCollection('user');
  var groupCol = self.store.getCollection('group');
  var receivers = [];
  var senderName;
  async.series([
    function(c) {
      userCol.findOne(
        {userId:msg.userId},
        function(err, doc) {
          if(err) return c(err);
          if(!doc) {
            err = 2;
            socket.emit('msgReply', {ret:2});
            return c(err);
          }
          senderName = doc.userName;
          c();
        });
    },
    function(c) {
      if(msgType === 0) {
        userCol.findOne(
          {userId:msg.id},
          function(err, doc) {
            if(err) return c(err);
            if(!doc) {
              err = 3;
              socket.emit('msgReply', {ret:3});
              return c(err);
            }
            receivers.push(msg.id);
            c();
          });
      } else {
        groupCol.findOne(
          {groupId:msg.id},
          function(err, doc) {
            if(err) return c(err);
            if(!doc) {
              err = 3;
              socket.emit('msgReply', {ret:3});
              return c(err);
            }
            if(doc.ownerId && doc.ownerId != msg.userId) {
              receivers.push(doc.ownerId);
            }
            if(doc.members) {
              doc.members.forEach(function(id) {
                if(id != msg.userId) receivers.push(id);
              });
            }
            c();
          });
      }
    },
    function(c) {
      var tasks = [];
      receivers.forEach(function(id) {
        tasks.push(function(cb) {
          var offline = 1;
          if(self.sockets[id]) {
            self.sockets[id].emit('msg', {
              msgType:msg.msgType,
              sender : msg.userId,
              id:msg.id,
              content:msg.content});
            offline = 0;
          }
          if(msgType === 0) {
            var chatCol = self.store.getCollection('chat');
            chatCol.insert({
              sender:msg.userId,
              senderName:senderName,
              receiver:id,
              msgContent:msg.content,
              offline : offline,
              ts : new Date() },[], function(err) {
                if(err) return c(err);
                cb();
              });
          } else {
            var groupChatCol = self.store.getCollection('groupchat');
            groupChatCol.insert({
              groupId:msg.id,
              sender:msg.userId,
              senderName:senderName,
              msgContent:msg.content,
              ts : new Date() }, [], function(err) {
                if(err) return c(err);
                cb();
              });
          }
        });
      });
      async.parallel(tasks, function() {
        c();
      });
    }
  ],
  function(err) {
    if(err) return; 
    socket.emit('msgReply', {ret:0});
  });
};

module.exports = ChatService;
