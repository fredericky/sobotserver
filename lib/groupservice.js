var async = require('async');
var mongodb = require('mongodb');

// constructor
function GroupService(opts) {
  this.store = opts.s;
}

// fetch group list
GroupService.prototype.fetchGroups = function(msg, res) {
  console.log('groupservice:fetchGroups:msg=', msg);
  var self = this;
  var groupCol = self.store.getCollection('group');
  var reply = {
    ret : 0
  };
  var data = [];
  async.series([
    function(c) {
      if(msg.sinceId == 'none') {
        groupCol.find().sort({_id:1}).limit(msg.length).toArray(function(e, arr) {
          if(e) return c(e);
          data = arr;
          c();
        });
      } else {
        var objId = mongodb.ObjectID(msg.sinceId);
        groupCol.find({_id:{$gt:objid}}).sort({_id:1}).limit(msg.length).toArray(function(e, arr) {
          if(e) return c(e);
          data = arr;
          c();
        });
      }
    },
    function(c) {
      if(data.length > 0) {
        reply.groupList = [];
        reply.lastId = data[data.length-1]._id;
        data.forEach(function(group) {
          delete group._id;
          group.size = group.members.length;
          delete group.members;
          reply.groupList.push(group);
        });
        c();
      }
    }
  ],
  function(err) {
    res.send(reply);
  });
};

// view a specfic group
GroupService.prototype.viewGroup = function(msg, res) {
  console.log('groupservice:viewGroup:msg=', msg);
  var self = this;
  var groupCol = self.store.getCollection('group');
  var reply = {
    ret : 0
  };
  var members = [];
  async.series([
    function(c) {
      groupCol.findOne({groupId:msg.groupId}, function(err, doc) {
        if(err) return c(err);
        members = doc.members;
        c();
      });
    },
    function(c) {
      var start;
      if(msg.sinceId == 'none') start = -1;
      else start = parseInt(msg.sinceId);
      var idList = [];
      var end = start+1 + msg.length;
      for(var i = start+1; i < members.length && i <= end; ++i) {
        idList.push(members[i]);
      }
      reply.lastId = start+1 + idList.length-1;
      var tasks = [];
      var memberList = [];
      idList.forEach(function(id) {
        tasks.push(function(cb) {
          var userCol = self.store.getCollection('user');
          userCol.findOne({userId:id}, function(err, doc) {
            if(err) return cb(err);
            var member = {memberId: id, memberName: doc.userName};
            memberList.push(member);
            cb();
          });
        });
      });
      async.parallel(tasks, function() {
        if(memberList.length !== 0) reply.memberList = memberList;
        c();
      });
    }
  ],
  function(err) {
    res.send(reply);
  });
};

// user create a group
GroupService.prototype.createGroup = function(msg, res) {
  console.log('groupservice:createGroup:msg=', msg);
  var self = this;
  var groupCol = self.store.getCollection('group');
  var reply = {
    ret : 0
  };
  var groupId;
  var ownerName;
  async.series([
    function(c) {
      // check owner existence
      var userCol = self.store.getCollection('user');
      userCol.findOne(
        {userId:msg.userId},
        function(err, doc) {
          if(err) return c(err);
          ownerName = doc.userName;
          c();
        });
    },
    function(c) {
      // check group existence
      groupCol.findOne(
        {groupName:msg.groupName},
        function(err, doc) {
          if(err) return c(err);
          if(doc) {
            err = 1;
            reply.ret = 2;
            return c(err);
          }
          c();
        });
    },
    function(c) {
      // allocate group id
      var countersCol = self.store.getCollection('counters');
      countersCol.findAndModify(
        {type:'group'},
        [],
        {$inc:{'seq':1}},
        {new:true},
        function(err, doc) {
          if(err) return c(err);
          groupId = doc.seq;
          c();
        });
    },
    function(c) {
      // construct a group object
      groupCol.findAndModify(
        {groupName:msg.groupName},
        [],
        {$setOnInsert: {
          groupId:groupId,
          ownerId:msg.userId,
          ownerName:ownerName,
          createTime : new Date(),
          updateTime : new Date()
        }},
        {upsert:true, new:true},
        function(err, doc) {
          if(err) return c(err);
          reply.groupId = groupId;
          c();
        });
    },
    function(c) {
      // add the group id to the user
      var userCol = self.store.getCollection('user');
      userCol.findAndModify(
        {userId:msg.userId},
        [],
        {$addToSet:{ownGroups:groupId}},
        {},
        function(err, doc) {
          if(err) return c(err);
          c();
        });
    }
  ],
  function(err) {
    res.send(reply);
  });
};

// user join a group
GroupService.prototype.joinGroup = function(msg, res) {
  console.log('groupservice:joinGroup=', msg);
  var self = this;
  var reply = {
    ret : 0
  };
  var userName;
  var userCol = self.store.getCollection('user');
  var groupCol = self.store.getCollection('group');
  async.series([
    function(c) {
      // check duplicated join
      userCol.findOne(
        {userId:msg.userId},
        function(err, doc) {
          if(err) return c(err);
          userName = doc.userName;
          var i;
          if(doc.ownGroups) {
            for(i = 0; i < doc.ownGroups.length; ++i) {
              if(doc.ownGroups[i] == msg.groupId) {
                err = 2;
                reply.ret = err;
                return c(err);
              }
            }
          }
          if(doc.joinGroups) {
            for(i = 0; i < doc.joinGroups.length; ++i) {
              if(doc.joinGroups[i] == msg.groupId) {
                err = 2;
                reply.ret = err;
                return c(err);
              }
            }
          }
          c();
        });
    },
    function(c) {
      // check group existence and add user to this group
      groupCol.findAndModify(
        {groupId:msg.groupId},
        [],
        {$addToSet:{members:msg.userId}},
        {new:false},
        function(err, doc) {
          if(err) return c(err);
          if(!doc) {
            err = 3;
            reply.ret = err;
            return c(err);
          }
          reply.groupId = msg.groupId;
          c();
        });
    },
    function(c) {
      // add group id to the user
      userCol.findAndModify(
        {userId:msg.userId},
        [],
        {$addToSet: {joinGroups:msg.groupId}},
        {},
        function(err, doc) {
          if(err) return c(err);
          c();
        });
    }
  ],
  function(err) {
    res.send(reply);
  });
};

// for user quit the group
GroupService.prototype.quitGroup = function(msg, res) {
  console.log('groupservice:quitGroup:msg=', msg);
  var self = this;
  var groupCol = self.store.getCollection('group');
  var reply = {
    ret : 0
  };
  async.series([
    function(c) {
      var userCol = self.store.getCollection('user');
      userCol.findOne(
        {userId:msg.userId},
        function(err, doc) {
          if(err) return c(err);
          var i = 0;
          // cannot quit a group created by himself
          if(doc.ownGroups) {
            for(; i < doc.ownGroups.length; ++i) {
              if(doc.ownGroups[i] == msg.groupId) {
                err = 2;
                reply.ret = err;
                return c(err);
              }
            }
          }
          // not join in the group
          if(!doc.joinGroups) {
            err = 3;
            reply.ret = err;
            return c(err);
          }
          console.log(doc.joinGroups);
          for(i = 0; i < doc.joinGroups.length; ++i) {
            if(doc.joinGroups[i] == msg.groupId) {
              break;
            }
          }
          if(i == doc.joinGroups.length) {
            err = 3;
            reply.ret = err;
            return c(err);
          }
          c();
        });
    },
    function(c) {
      var userCol = self.store.getCollection('user');
      userCol.findAndModify(
        {userId : msg.userId},
        [],
        {$pull : {joinGroups:msg.groupId}},
        {new:true},
        function(err, doc) {
          if(err) return c(err);
          c();
        });
    },
    function(c) {
      var groupCol = self.store.getCollection('group');
      groupCol.findAndModify(
        {groupId : msg.groupId},
        [],
        {$pull : {members:msg.userId}},
        {new:true},
        function(err, doc) {
          if(err) return c(err);
          c();
        });
    }
  ],
  function(err) {
    res.send(reply);
  });
};

module.exports = GroupService;
