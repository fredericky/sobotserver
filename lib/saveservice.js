var async = require('async');
var mongodb = require('mongodb');

// construct of save service
function SaveService(opts) {
  this.store = opts.s;
}

// fetch save list
SaveService.prototype.fetchSaveList = function(msg, res) {
  console.log("saveservice:fetchSaveList:msg=", msg);
  var self = this;
  var reply = {
    ret : 0
  };
  var data = [];
  var saveCol = self.store.getCollection('save');
  async.series([
    function(c) {
      if(msg.sinceId == "none") {
        saveCol.find({userId:msg.userId}).sort({_id:1}).limit(msg.length).
          toArray(function(err, arr) {
          if(err) return c(err);
          data = arr;
          c();
        });
      } else {
        var objId = mongodb.ObjectID(msg.sinceId);
        saveCol.find({_id:{$gt:objId}}).sort({_id:1}).limit(msg.length).
          toArray(function(err, arr) {
          if(err) return c(err);
          data = arr;
          c();
        });
      }
    },
    function(c) {
      if(data && data.length > 0) {
        reply.saveList = [];
        reply.lastId = data[data.length-1]._id;
        data.forEach(function(item) {
          delete item.userId;
          delete item.userName;
          reply.saveList.push(item);
        });
      }
      c();
    }
  ],
  function(err) {
    res.send(reply);
  });
};

// use takes a save action
SaveService.prototype.save = function(msg, res) {
  console.log("saveservice:save:msg=", msg);
  var self = this;
  var saveCol = self.store.getCollection('save');
  var userCol = self.store.getCollection('user');
  var reply = {
    ret : 0
  };
  var userName, peerName;
  async.series([
    function(c) {
      userCol.findOne(
        {userId : msg.userId},
        function(err, doc) {
          if(err) return c(err);
          if(!doc) {
            err = 1;
            reply.ret = err;
            return c(err);
          }
          userName = doc.userName;
          c();
        });
    },
    function(c) {
      userCol.findOne(
        {userId : msg.peerId},
        function(err, doc) {
          if(err) return c(err);
          if(!doc) {
            err = 2;
            reply.ret = 2;
            return c(err);
          }
          peerName = doc.userName;
          c();
        });
    },
    function(c) {
      saveCol.findOne(
        {userId : msg.userId, peerId : msg.peerId},
        function(err, doc) {
          if(err) return c(err);
          if(doc) {
            err = 3;
            reply.ret = err;
            return c(err);
          }
          c();
        });
    },
    function(c) {
      saveCol.findAndModify(
        {userId : msg.userId, peerId : msg.peerId},
        [],
        {$setOnInsert : {
          userId : msg.userId,
          userName : userName,
          peerId : msg.peerId,
          peerName : peerName,
          createTime : new Date()}},
        {upsert : true},
        function(err, doc) {
          if(err) return c(err);
          c();
        });
    }
  ],
  function(err) {
    res.send(reply);
  });
};

// use cancels save
SaveService.prototype.cancelSave = function(msg, res) {
  console.log("saveservice:cancelsave:msg=", msg);
  var self = this;
  var saveCol = self.store.getCollection('save');
  var reply = {
    ret : 0
  };
  async.series([
    function(c) {
      var userCol = self.store.getCollection('user');
      userCol.findOne(
        {userId : msg.peerId},
        function(err, doc) {
          if(err) return c(err);
          if(!doc) {
            err = 2;
            reply.ret = err;
            return c(err);
          }
          c();
        });
    },
    function(c) {
      saveCol.findOne(
        {userId : msg.userId, peerId : msg.peerId},
        function(err, doc) {
          if(err) return c(err);
          if(!doc) {
            err = 3;
            reply.ret = err;
            return c(err);
          }
          c();
        });

    },
    function(c) {
      saveCol.remove(
        {userId : msg.userId, peerId : msg.peerId},
        function(err, doc) {
          if(err) return c(err);
          c();
        });
    }
  ],
  function(err) {
    res.send(reply);
  });
};

module.exports = SaveService;
