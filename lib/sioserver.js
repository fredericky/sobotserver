var sio = require('socket.io');
var async = require('async');

var ChatService = require('./chatservice');
var Store = require('./store');

var config = require('./config');

var store;
var chatService;
var sockets = {};

// start a long connection based server
module.exports.start = function(argv) {
  async.series([
    function(c) {
      store = new Store(function(err) {
        if(err) {
          console.error('sioserver:fail to connect to mongodb');
          return c(err);
        }
        c();
      });
    },
    function(c) {
      chatService = new ChatService({s:store, sockets:sockets});
      c();
    },
    function(c) {
      var httpServer = require('http').createServer();
      var server = sio(httpServer);
      httpServer.listen(config.sioServerPort, function() {
        console.log('sio start at port %d', config.sioServerPort);
      });
      server.on('connection', function(socket) {
        var userId;

        socket.emit('challenge', {msg:'hello'});
        socket.on('response', function(data) {
          console.log('reponse=', data);
          if(sockets[data.userId]) {
            console.log('userId=%d already existed', data.userId);
            sockets[data.userId].disconnect(true);
          } else {
            userId = data.userId;
            sockets[data.userId] = socket;
          }
        });

        socket.on('msg', function(data) {
          chatService.sendMsg(data, socket);
        });

        socket.on('disconnect', function() {
          console.log('userId=%d disconnected!', userId);
          if(socket[userId]) sockets[userId].disconnect(true);
        });

      });
      c();
    }
  ],
  function(err) {
    if(err) console.log(err);
  });
};

