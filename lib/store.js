var mongodb = require('mongodb');
var async = require('async');

var config = require('./config');

// constructor of store
function Store(cb) {
  var self = this;
  self.connect(function(err, conn) {
    if(err) return cb(err);
    cb();
  });
}

// connect for mongodb
Store.prototype.connect = function(cb) {
  var self = this;
  async.series([
    function(c) {
      // connect to the mongodb
      mongodb.MongoClient.connect(config.dbUrl, function(err, conn) {
        if(err) return c(err);
        self.dbConn = conn;
        c();
      });
    },
    function(c) {
      // TODO: set index
      c();
    },
  ], cb);
};

// get a specific collection
Store.prototype.getCollection = function(tableName) {
  return this.dbConn.collection(tableName);
};

module.exports = Store;
