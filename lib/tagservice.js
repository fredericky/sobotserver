var async = require('async');
var mongodb = require('mongodb');

// constructor of tag service
function TagService(opts) {
  this.store = opts.s;
}

// fetch all tag list
TagService.prototype.fetchAllTags = function(msg, res) {
  console.log('tagservice:fetchAllTag:msg=', msg);
  var self = this;
  var userCol = self.store.getCollection('user');
  var tagCol = self.store.getCollection('tag');
  var reply = {
    ret : 0,
  };
  var data = [];
  async.series([
   function(c) {
      if(msg.sinceId == "none") {
        tagCol.find().sort({_id:1}).limit(msg.length).toArray(function(err, arr) {
          if(err) return c(err);
          data = arr;
          c();
        });
      } else {
        var objId = mongodb.ObjectID(msg.sinceId);
        tagCol.find({_id:{$gt : objid}}).sort({_id:1}).limit(msg.length).toArray(function(err, arr) {
          if(err) return c(err);
          data = arr;
          c();
        });
      }
    },
    function(c) {
      if(data.length > 0) {
        reply.tagList = [];
        reply.lastId = data[data.length-1]._id;
        data.forEach(function(tag) {
          delete tag._id;
          reply.tagList.push(tag);
        });
      }
      c();
    }
  ],
  function(err) {
    res.send(reply);
  });
};

// create a tag
TagService.prototype.createTag = function(msg, res) {
  console.log('tagservice:createtag:msg=', msg);
  var self = this;
  var userCol = self.store.getCollection('user');
  var tagCol = self.store.getCollection('tag');
  var reply = {
    ret : 0
  };
  var userName;
  async.series([
    function(c) {
      userCol.findOne(
        {userId : msg.userId},
        function(err, doc) {
          if(err) return c(err);
          if(!doc) {
            err = 1;
            reply.ret = 1;
            return c(err);
          }
          userName = doc.userName;
          c();
        });
    },
    function(c) {
      tagCol.findOne(
        {tag : msg.tag},
        function(err, doc) {
          if(err) return c(err);
          if(doc) {
            err = 2;
            reply.ret = 2;
            return c(err);
          }
          c();
        });
    },
    function(c) {
      var delTagCol = self.store.getCollection('deltag');
      delTagCol.remove(
        {tag : msg.tag},
        function(err, doc) {
          if(err) return c(err);
          c();
        });
    },
    function(c) {
      tagCol.findAndModify(
        {tag : msg.tag},
        [],
        {$setOnInsert : {
          tag : msg.tag, 
          ownerId : msg.userId, 
          ownerName : userName, 
          createTime : new Date()}},
        {upsert : true},
        function(err, doc) {
          if(err) return c(err);
          c();
        });
    },
    function(c) {
      userCol.findAndModify(
        {userId : msg.userId},
        [],
        {$addToSet: {tags : msg.tag}},
        {},
        function(err, doc) {
          if(err) return c(err);
          c();
        });
    },
  ],
  function(err) {
    res.send(reply);
  });
};

// share a tag
TagService.prototype.shareTag = function(msg, res) {
  console.log("tagservice:shareTag:msg=", msg);
  var self = this;
  var userCol = self.store.getCollection('user');
  var tagCol = self.store.getCollection('tag');
  var reply = {
    ret : 0
  };
  async.series([
   function(c) {
      tagCol.findOne(
        {tag : msg.tag},
        function(err, doc) {
          if(err) return c(err);
          if(!doc) {
            err = 2;
            reply.ret = err;
            return c(err);
          }
          c();
        });
    },
    function(c) {
      userCol.findAndModify(
        {userId : msg.userId},
        [],
        {$addToSet: {tags : msg.tag}},
        {new:false},
        function(err, doc) {
          if(err) return c(err);
          if(doc.tags.length > 0) {
            for(var i = 0; i < doc.tags.length; ++i) {
              if(doc.tags[i] == msg.tag) {
                err = 3;
                reply.ret = err;
                return c(err);
              }
            }
          }
          c();
        });
    }
 ],
  function(err) {
    res.send(reply);
  });
};

// remove a tag
TagService.prototype.removeTag = function(msg, res) {
  console.log("tagservice:removeTag:msg=", msg);
  var self = this;
  var userCol= self.store.getCollection('user');
  var tagCol = self.store.getCollection('tag');
  var reply = {
    ret : 0
  };
  async.series([
    function(c) {
      tagCol.findOne(
        {tag : msg.tag},
        function(err, doc) {
          if(err) return c(err);
          if(!doc) {
            err = 2;
            reply.ret = err;
            return c(err);
          }
          if(doc.ownerId != msg.userId) {
            err = 3;
            reply.ret = err;
            return c(err);
          }
          c();
        });
    },
    function(c) {
      tagCol.remove(
        {tag : msg.tag},
        function(err, doc) {
          if(err) return c(err);
          c();
        });
    },
    function(c) {
      delTagCol = self.store.getCollection('deltag');
      delTagCol.findAndModify(
        {tag : msg.tag},
        [],
        {$setOnInsert : {tag : msg.tag}},
        {upsert : true},
        function(err, doc) {
          if(err) return c(err);
          c();
        });
    }
  ],
  function(err) {
    res.send(reply);
  });
};

module.exports = TagService;
