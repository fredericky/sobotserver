var async = require('async');

var config = require('./config');

// construtor of userservice
function UserService(opts) {
  this.store = opts.s;
  this.tagService = opts.tagService;
  this.saveService = opts.saveService;
  this.groupService = opts.groupService;
  this.chatService = opts.chatService;
}

// register
UserService.prototype.register = function(msg, res) {
  console.log('userservice:register:msg=', msg);
  var self = this;
  var userCol = self.store.getCollection('user');
  var user = {
    userId : 0,
    userName : msg.userName,
    password : msg.password,
    email : msg.email,
    firstName : msg.firstName,
    lastName : msg.lastName,
    organization : msg.organization,
    works : msg.works,
    avatar : msg.avatar,
    title : msg.title
  };
  var reply = {
    ret : 0
  };
  var userId = 0;
  async.series([
    function(c) {
      userCol.findOne(
        {userName : msg.userName},
        function(err, doc) {
          if(doc) {
            err = 1;
            reply.ret = 1;
            return c(err);
          }
          c();
        });
    },
    function(c) {
      userCol.findAndModify(
        {userName : msg.userName},
        [],
        {$setOnInsert : user},
        {upsert:true, new: true},
        function(err, doc) {
          if(err) return c(err);
          c();
        });
    },
    function(c) {
      var countersCol = self.store.getCollection('counters');
      countersCol.findAndModify(
        {type:'user'},
        [],
        {$inc:{'seq':1}},
        {new:true},
        function(err, doc) {
          if(err) return c(err);
          userId = doc.seq;
          c();
        });
    },
    function(c) {
      userCol.findAndModify(
        {userName:msg.userName},
        [],
        {$set:{userId:userId}},
        {new:true},
        function(err, doc) {
          if(err) return c(err);
          c();
        });
    }
  ],
  function(err) {
    res.send(reply);
  });
};

// user login
UserService.prototype.login = function(msg, res) {
  console.log('userservice:login:msg=', msg);
  var self = this;
  var reply = {
    ret : 0
  };
  var userCol = self.store.getCollection('user');
  async.series([
    function(c) {
      userCol.findOne(
        {userName:msg.userName},
        function(err, doc) {
          if(err) return c(err);
          if(!doc) {
            err = 1;
            reply.ret = err;
            return c(err);
          }
          if(doc.password != msg.password) {
            err = 2;
            reply.ret = err;
            return c(err);
          }
          reply.userId = doc.userId;
          reply.email = doc.email;
          reply.firstName = doc.firstName;
          reply.lastName = doc.lastName;
          reply.organization = doc.organization;
          reply.works = doc.works;
          reply.avatar = doc.avatar;
          reply.title = doc.title;
          reply.chatServer = config.chatServer;
          if(doc.tags) {
            reply.tags = doc.tags;
          }
          c();
        });
    },
    function(c) {
      // filter the removed tags
      if(!reply.tags || reply.tags.length === 0) {
        c();
        return;
      }
      var tasks = [];
      var tags = [];
      reply.tags.forEach(function(tag) {
        tasks.push(function(cb) {
          var deltagCol = self.store.getCollection('deltag');
          deltagCol.findOne(
            {tag : tag},
            function(err, doc) {
              if(err) return cb(err);
              if(!doc) tags.push(tag);
              cb();
            });
        });
      });
      async.parallel(tasks, function() {
        reply.tags = tags;
        c();
      });
    },
    function(c) {
      userCol.findAndModify(
        {userName:msg.userName},
        [],
        {$set:{lastLoginTime:new Date()}},
        {},
        function(err, doc) {
          if(err) return c(err);
          c();
        });
    }
  ],
  function(err) {
    res.send(reply);
  });
};

// view a specfic user
UserService.prototype.viewUser = function(msg, res) {
  console.log('userservice:viewUser=', msg);
  var self = this;
  var reply = {
    ret : 0
  };
  if(msg.userId == msg.otherUserId) {
    reply.ret = 2;
    res.send(reply);
    return;
  }
  var userCol = self.store.getCollection('user');
  async.series([
    function(c) {
      userCol.findOne(
        {userId : msg.otherUserId},
        function(err, doc) {
          if(err) return c(err);
          if(!doc) {
            err = 3;
            reply.ret = err;
            return c(err);
          }
          reply.userName = doc.userName;
          reply.email = doc.email;
          reply.firstName = doc.firstName;
          reply.lastName = doc.lastName;
          reply.organization = doc.organization;
          reply.works = doc.works;
          reply.avatar = doc.avatar;
          reply.title = doc.title;
          if(doc.tags) {
            reply.tags = doc.tags;
          }
          c();
        });
    },
    function(c) {
      // filter the removed tags
      if(!reply.tags || reply.tags.length === 0) {
        c();
      }
      var tasks = [];
      var tags = [];
      reply.tags.forEach(function(tag) {
        tasks.push(function(cb) {
          var deltagCol = self.store.getCollection('deltag');
          deltagCol.findOne(
            {tag : tag},
            function(err, doc) {
              if(err) return cb(err);
              if(!doc) tags.push(tag);
              cb();
            });
        });
      });
      async.parallel(tasks, function() {
        reply.tags = tags;
        c();
      });
    }
  ],
  function(err) {
    res.send(reply);
  });
};

UserService.prototype.userAction = function(msg, res) {
  var self = this;
  var ret = 0;
  async.series([
    function(c) {
      var userCol = self.store.getCollection('user');
      userCol.findOne(
        {userId : msg.userId},
        function(err, doc) {
          if(err) return c(err);
          if(!doc) {
            err = 1;
            ret = err;
            return c(err);
          }
          c();
        });
    },
    function(c) {
      switch(msg.type) {
        case 3:
          self.viewUser(msg, res);
          break;
        case 11:
          self.tagService.fetchAllTags(msg, res);
          break;
        case 12:
          self.tagService.createTag(msg, res);
          break;
        case 13:
          self.tagService.shareTag(msg, res);
          break;
        case 14:
          self.tagService.removeTag(msg, res);
          break;
        case 21:
          self.groupService.fetchGroups(msg, res);
          break;
        case 22:
          self.groupService.viewGroup(msg, res);
          break;
        case 23:
          self.groupService.createGroup(msg, res);
          break;
        case 24:
          self.groupService.joinGroup(msg, res);
          break;
        case 25:
          self.groupService.quitGroup(msg, res);
          break;
        case 31:
          self.saveService.save(msg, res);
          break;
        case 32:
          self.saveService.cancelSave(msg, res);
          break;
        case 33:
          self.saveService.fetchSaveList(msg, res);
          break;
        case 34:
          self.chatService.fetchChatMsgList(msg, res);
          break;
        default:
          console.log("unknow protocol:msg=", msg);
      }
      c();
    }
  ],
  function(err) {
    if(err) res.send({ret : ret});
  });
};

module.exports =UserService;
