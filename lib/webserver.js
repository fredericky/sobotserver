var express = require('express');
var http = require('http');
var app = express();
var bodyParser = require('body-parser');
var async = require('async');
var Store = require('./store');
var UserService = require('./userservice');
var TagService = require('./tagservice');
var SaveService = require('./saveservice');
var GroupService = require('./groupservice');
var ChatService = require('./chatservice');

var config = require('./config');

var store;
var userService;
var tagService;
var saveService;
var groupService;
var chatService;

app.use(bodyParser.json());

app.use('/sobot/', express.static('public'));

app.post('/sobot/request', handleRequest);

// the handler for the request from the client
function handleRequest(req, res) {
  var msg = req.body;
  res.set('Content-Type', 'application/json');
  switch(msg.type) {
    case 1:
      userService.register(msg, res);
      break;
    case 2:
      userService.login(msg, res);
      break;
    default:
      userService.userAction(msg, res);
  }
}

// start the webserver
module.exports.start =  function(argv) {
  var server = http.Server(app);
  async.series([
    function(c) {
      store = new Store(function(err) {
        if(err) {
          console.err('fail to connect to mongodb');
          return c(err);
        }
        c();
      });
    },
    function(c) {
      var countersCol = store.getCollection('counters');
      countersCol.findAndModify(
        {type:'user'},
        [],
        {$setOnInsert:{type:'user',seq:0}},
        {upsert:true},
        function(err, doc) {
          if(err) {
            console.err('fail to init user seq');
            return c(err);
          }
          c();
        });
    },
    function(c) {
      var countersCol = store.getCollection('counters');
      countersCol.findAndModify(
        {type:'group'},
        [],
        {$setOnInsert:{type:'group',seq:0}},
        {upsert:true},
        function(err, doc) {
          if(err) {
            console.err('fail to init group seq');
            return c(err);
          }
          c();
        });
    }
  ],
  function(err) {
    if(err) {
      console.error(err);
      return;
    }
    tagService = new TagService({s:store});
    saveService = new SaveService({s:store});
    groupService = new GroupService({s:store});
    chatService = new ChatService({s:store});
    userService = new UserService({
      s:store, 
      tagService:tagService,
      saveService:saveService,
      groupService:groupService,
      chatService:chatService
    });

    console.log('webserver started');
    server.listen(argv.p || config.webServerPort);
  });
};
