var ioc = require('socket.io-client');

function client(nsp, opts) {
  if('object' == typeof nsp) {
    opts = nsp;
    nsp = null;
  }
  var url = 'ws://localhost:8001' + (nsp || '');
  return ioc(url, opts);
}

var socket = client();
socket.on('challenge', function(msg) {
  console.log('server msg', msg);
  socket.emit('response', {userId:1});
  socket.emit('msg', {msgType:1, userId:1, id:1, content: 'hello'});
});

socket.on('msgReply', function(msg) {
  console.log('msgReply=', msg);
});

var socket2 = client();
socket.on('challenge', function(msg) {
  console.log('server msg', msg);
  socket.emit('response', {userId:2});
});

socket.on('msg', function(msg) {
  console.log('msg=', msg);
});



